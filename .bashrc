#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


####################
##### Aliases ######
####################

ArchUpdate () {
  yay -Syyu; flatpak update
}

ArchUpdateFirmware () {
  sudo fwupdmgr get-devices && sudo fwupdmgr refresh --force && sudo fwupdmgr get-updates && sudo fwupdmgr update
}

ArchClean () {
  sudo pacman -Qtdq | sudo pacman -Rns - ; sudo paccache -rk 1 ; sudo paccache -ruk0
}

ArchFixKeyring () {
  sudo pacman -S archlinux-keyring && sudo pacman-key --refresh-keys
}

ls () {
  command ls --color=auto $@
}

ll () {
  ls -lh $@
}

lla () {
  ls -lah $@
}

filesize () {
  du -sh * .[!.]*
}

grep () {
  command grep --color=auto $@
}

####################
#### Aliases END ###
####################


####################
#### Functions  ####
####################

wghome ()
{
  # Toggles the tunnel
  if sudo wg show wghome &>/dev/null; then
    echo "WireGuard tunnel is active. Stopping..."
    #sudo wg-quick down wghome
    sudo nmcli connection down wghome
  else
    echo "WireGuard tunnel is not active. Starting..."
    #sudo wg-quick up wghome
    sudo nmcli connection up wghome
  fi
}

####################
## Functions END ###
####################


####################
#### ENV VARS ######
####################

# Default Editor
export EDITOR=nvim
# Format of default prompt
PS1='[\u@\h \W]\$ '
# Exporting this env makes is so that every time a command is entered
# it is written into the history and then the history gets reloaded
export PROMPT_COMMAND='history -a; history -c; history -r'
# This env defines the length of the history file
export HISTSIZE=1000000
# Makes nvim the default for Man pages
export MANPAGER='nvim +Man!'
export MANWIDTH=999

####################
### ENV VARS END ###
####################


####################
###### MISC ########
####################

# Needed for gpg to work with git commits
export GPG_TTY=$(tty)

# Activate vi mode
# set -o vi

# Loads Starship Prompt (should be at the end)
eval "$(starship init bash)"

# Set up fzf key bindings and fuzzy completion
eval "$(fzf --bash)"

# Set up thefuck
eval "$(thefuck --alias)"

# Activate conda
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh
export CRYPTOGRAPHY_OPENSSL_NO_LEGACY=1

####################
#### MISC END ######
####################


###############################
##### Source extra files ######
###############################

if [ -f ~/.bashrc_extention ]; then
    source ~/.bashrc_extention
fi

