#!/bin/bash
# Installs pacman packages
sudo pacman -S --needed base-devel neovim tmux stow tmux ripgrep npm git starship curl wget unzip tar gzip luarocks cppcheck gdb lazygit gcc clang make fzf zoxide thefuck lldb python-setuptools go

# Checks if yay is installed and installs it if not
if ! command -v yay &> /dev/null; then
    git clone https://aur.archlinux.org/yay.git ~/yay
    (cd ~/yay && makepkg -si)
fi
# Installs yay packages
yay --needed -S tmux-bash-completion-git

# Checks if tpm is installed and installs it if not
if [ ! -e "$HOME/.tmux/plugins/tpm/tpm" ]; then
    mkdir -p "$HOME/.tmux/plugins"
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi
