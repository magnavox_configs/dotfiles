-- Sets Leader to Space
vim.g.mapleader = " "
-- File Explorer
-- vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
-- Swap ; and , in normal mode
vim.keymap.set("n", ";", ",", { noremap = true, silent = true })
vim.keymap.set("n", ",", ";", { noremap = true, silent = true })
-- Make HOME key to ^ insdead of 0
vim.keymap.set("n", "<HOME>", "^")
-- Ctrl Arrow Key behaviour
vim.keymap.set("n", "<C-RIGHT>", "w")
vim.keymap.set("n", "<C-LEFT>", "b")
-- When going half page up and down
-- It keeps the cursor vertically centered
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
-- When searching
-- It keeps the cursor vertically centered
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
-- Map * (selects current word for search) to ctrl-n
-- vim.keymap.set("n", "<C-_>", "*")
vim.keymap.set("n", "<leader>/", "*", { noremap = true, silent = true })
-- added macro capability for buffers where q is already used like e.g. oil.nvim
vim.keymap.set("n", "<leader>q", "q", { noremap = true })
-- window navigation
vim.keymap.set("n", "<leader>%", ":split<CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>=", ":vsplit<CR>", { noremap = true, silent = true })
-- System clipboard copy and paste
vim.keymap.set("n", "<leader>y", '"+y', { noremap = true, silent = true })
vim.keymap.set("v", "<leader>y", '"+y', { noremap = true, silent = true })
vim.keymap.set("n", "<leader>p", '"+p', { noremap = true, silent = true })
vim.keymap.set("v", "<leader>p", '"+p', { noremap = true, silent = true })
vim.keymap.set("n", "<leader>P", '"+P', { noremap = true, silent = true })
vim.keymap.set("v", "<leader>P", '"+P', { noremap = true, silent = true })
-- Move line up and down
vim.keymap.set("n", "J", function()
  -- This function moves the current line down by the count
  -- and then reindents the line
  -- it checks if the target line is out of bounds
  -- this is useful for moving the current line to the last line
  local count = vim.v.count1
  local current_line = vim.fn.line(".")
  local last_line = vim.fn.line("$")
  local target = current_line + count
  if target > last_line then
    target = last_line
  end
  vim.cmd("m " .. target)
  vim.cmd("normal! ==")
end, { noremap = true, silent = true })

vim.keymap.set("n", "K", function()
  -- This function moves the current line up by the count
  -- and then reindents the line
  -- it checks if the target line is out of bounds
  -- this is useful for moving the current line to the fist line
  local count = vim.v.count1
  local current_line = vim.fn.line(".")
  local target = current_line - (count + 1)
  if target < 0 then
    target = 0
  end
  vim.cmd("m " .. target)
  vim.cmd("normal! ==")
end, { noremap = true, silent = true })

-- LSP
-- Makes sure the keymaps are only applied if a LSP-Server is attached
-- Can be made more robust with checks to see if the current LSP-Server
-- has the capability or not see ":h LspAttach"
vim.api.nvim_create_autocmd("LspAttach", {
  callback = function(e)
    local opts = { buffer = e.bufnr }
    vim.keymap.set("n", "gh", function()
      vim.lsp.buf.hover()
    end, opts)
    vim.keymap.set("n", "gd", function()
      vim.lsp.buf.definition()
    end, opts)
    vim.keymap.set("n", "gD", function()
      vim.lsp.buf.declaration()
    end, opts)
    vim.keymap.set("n", "gi", function()
      vim.lsp.buf.implementation()
    end, opts)
    vim.keymap.set("n", "go", function()
      vim.lsp.buf.type_definition()
    end, opts)
    vim.keymap.set("n", "gr", function()
      vim.lsp.buf.references()
    end, opts)
    vim.keymap.set("n", "gs", function()
      vim.lsp.buf.signature_help()
    end, opts)
    vim.keymap.set("n", "<F2>", function()
      vim.lsp.buf.rename()
    end, opts)
    --vim.keymap.set('n', '<F3>', function() vim.lsp.buf.format() end, opts)  This is handled by conform
    vim.keymap.set("n", "<F4>", function()
      vim.lsp.buf.code_action()
    end, opts)
    vim.keymap.set("n", "gl", function()
      vim.diagnostic.open_float()
    end, opts)
    vim.keymap.set("n", "<space>wa", function()
      vim.lsp.buf.add_workspace_folder()
    end, opts)
    vim.keymap.set("n", "<space>wr", function()
      vim.lsp.buf.remove_workspace_folder()
    end, opts)
  end,
})
