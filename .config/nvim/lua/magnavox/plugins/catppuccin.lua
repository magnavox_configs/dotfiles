-- Catppuccin (Colorscheme)
return {
  --
  "catppuccin/nvim",
  lazy = false,
  name = "catppuccin",
  priority = 1000,
  config = function()
    -- Load catppuccin with defaults
    vim.cmd.colorscheme("catppuccin-mocha")
  end,
}
