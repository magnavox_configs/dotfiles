-- Debug Adapter Protocol (Debugging)
return {
  {
    "mfussenegger/nvim-dap",
    dependencies = {
      "rcarriga/nvim-dap-ui",
      "nvim-neotest/nvim-nio",
      "theHamsta/nvim-dap-virtual-text",
    },
    config = function()
      local dap, dapui = require("dap"), require("dapui")
      -- Virtual Text
      require("nvim-dap-virtual-text").setup({})

      -- C/C++/Rust Start --

      -- Adapter
      -- Start codelldb
      dap.adapters.codelldb = {
        type = "server",
        port = "${port}",
        executable = {
          -- CHANGE THIS to your path!
          command = "/usr/bin/codelldb",
          args = { "--port", "${port}" },
        },
      }
      -- Configurations
      dap.configurations.c = {
        {
          name = "Launch file",
          type = "codelldb",
          request = "launch",
          cwd = "${workspaceFolder}",
          program = function()
            return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
          end,
          args = function()
            local args_string = vim.fn.input("Input arguments: ")
            if args_string == "" then
              return nil
            else
              return vim.split(args_string, " ")
            end
          end,
          stopOnEntry = false,
        },
      }

      dap.configurations.cpp = dap.configurations.c
      dap.configurations.rust = dap.configurations.c

      -- C/C++/Rust End --

      -- DAP-UI Setup
      require("dapui").setup()
      dap.listeners.before.attach.dapui_config = function()
        dapui.open()
      end
      dap.listeners.before.launch.dapui_config = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated.dapui_config = function()
        dapui.close()
      end
      dap.listeners.before.event_exited.dapui_config = function()
        dapui.close()
      end

      -- Keymaps
      vim.keymap.set("n", "<Leader>dbt", function()
        dap.toggle_breakpoint()
      end)
      vim.keymap.set("n", "<Leader>dc", function()
        dap.continue()
      end)
    end,
  },
  {
    "mfussenegger/nvim-dap-python",
    ft = "python",
    config = function()
      local path = "~/.local/share/nvim/mason/packages/debugpy/venv/bin/python"
      require("dap-python").setup(path)
    end,
  },
  {
    "leoluz/nvim-dap-go",
    ft = "go",
    config = function()
      require("dap-go").setup()
      -- Optional: Map a key for debugging the nearest Go test.
      -- vim.keymap.set("n", "<Leader>dt", function()
      --   require("dap-go").debug_test()
      -- end, { desc = "Debug nearest Go test" })
    end,
  },
}
