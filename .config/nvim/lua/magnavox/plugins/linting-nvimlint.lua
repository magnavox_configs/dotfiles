-- nvim-lint (Linting)
return {
  "mfussenegger/nvim-lint",
  event = { "BufReadPre", "BufNewFile" },
  config = function()
    require("lint").linters_by_ft = {
      lua = { "luacheck" },
      python = { "ruff", "pylint" }, -- pylint is used for import checking ruff for the rest
      sh = { "shellcheck" },
      terraform = { "tflint" },
      go = { "golangcilint" },
      css = { "stylelint" },
      html = { "htmlhint" },
      markdown = { "markdownlint" },
    }

    vim.api.nvim_create_autocmd(
      { "BufReadPost", "BufNewFile", "BufEnter", "InsertLeave", "BufWritePost" },
      {
        callback = function()
          require("lint").try_lint()
        end,
      }
    )
  end,
}
