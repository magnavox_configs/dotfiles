-- Conform (Formatting)
return {
  "stevearc/conform.nvim",
  event = { "BufReadPre", "BufNewFile" },
  config = function()
    require("conform").setup({
      formatters_by_ft = {
        lua = { "stylua" },
        python = { "ruff" },
        markdown = { "mdformat" },
        yaml = { "prettier" },
        sh = { "beautysh" },
        -- this is called hcl instead of hclfmt because of a missmatch between the binary name and the file extension
        terraform = { "hcl" }, --hclfmt
        go = { "goimports" },
        html = { "prettier" },
        css = { "prettier" },
      },
      -- Format on Save
      format_on_save = {
        lsp_format = "fallback",
        timeout_ms = 1000,
      },
      -- Keymaps
      vim.keymap.set("n", "<F3>", function()
        require("conform").format({
          lsp_format = "fallback",
          timeout_ms = 1000,
        })
      end, {}),
    })
  end,
}
