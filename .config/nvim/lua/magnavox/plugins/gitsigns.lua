-- Gitsigns (Git decorations, eg. hunk preview, line blame, ...)
return {
  "lewis6991/gitsigns.nvim",
  config = function()
    require("gitsigns").setup()
    -- Git Preview
    vim.keymap.set("n", "<leader>gp", ":Gitsigns preview_hunk<CR>", {})
    vim.keymap.set("n", "<leader>gb", ":Gitsigns toggle_current_line_blame<CR>", {})
  end,
}
