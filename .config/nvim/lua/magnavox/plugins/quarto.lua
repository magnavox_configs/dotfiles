-- plugins/quarto.lua
return {
  {
    "quarto-dev/quarto-nvim",
    dependencies = {
      "jmbuhr/otter.nvim",
      "nvim-treesitter/nvim-treesitter",
    },
    config = function()
      require("quarto").setup()
      -- Keymaps
      local runner = require("quarto.runner")
      vim.keymap.set("n", "<leader>rc", runner.run_cell, { silent = true })
      vim.keymap.set("n", "<leader>ra", runner.run_above, { silent = true })
      vim.keymap.set("n", "<leader>rA", runner.run_all, { silent = true })
      vim.keymap.set("n", "<leader>rl", runner.run_line, { silent = true })
      vim.keymap.set("v", "<leader>r", runner.run_range, { silent = true })
      vim.keymap.set("n", "<leader>RA", function()
        runner.run_all(true)
      end, { silent = true })
    end,
  },
}
