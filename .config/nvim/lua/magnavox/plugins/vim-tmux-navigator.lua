-- nvim-tmux-navigator (For navigating between nvim and tmux)
return {
  "christoomey/vim-tmux-navigator",
  init = function()
    -- Disable default Ctrl-based mappings
    vim.g.tmux_navigator_no_mappings = 1
  end,
  cmd = {
    "TmuxNavigateLeft",
    "TmuxNavigateDown",
    "TmuxNavigateUp",
    "TmuxNavigateRight",
    "TmuxNavigatePrevious",
  },
  keys = {
    { "<A-h>", "<cmd>TmuxNavigateLeft<cr>" },
    { "<A-j>", "<cmd>TmuxNavigateDown<cr>" },
    { "<A-k>", "<cmd>TmuxNavigateUp<cr>" },
    { "<A-l>", "<cmd>TmuxNavigateRight<cr>" },
    { "<A-J>", "<cmd>TmuxNavigatePrevious<cr>" },
  },
}
