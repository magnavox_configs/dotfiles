-- plugins/otter.lua
return {
  {
    "jmbuhr/otter.nvim",
    config = function()
      require("otter").setup({
        buffers = {
          set_filetype = true, -- Ensures extracted buffers get the proper filetype (e.g., "python")
          write_to_disk = false, -- (Optional) keeps otter buffers in memory
        },
      })
    end,
  },
}
