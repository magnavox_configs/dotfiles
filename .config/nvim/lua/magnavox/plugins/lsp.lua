-- LSP Configuration (lspconfig, mason, nvim-cmp, LuaSnip, Copilot)
return {
  -- Is its own tabel because of the make command
  {
    "L3MON4D3/LuaSnip",
    -- install jsregexp (optional!).
    build = "make install_jsregexp",
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      -- Mason and Mason related plugins
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "WhoIsSethDaniel/mason-tool-installer.nvim",
      -- Added UI-Element to show status of LSP (Lower right hand corner)
      "j-hui/fidget.nvim",
      -- Ensure CMP-Related
      "hrsh7th/nvim-cmp",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/nvim-cmp",
      -- Ensure Snippets Engine
      "L3MON4D3/LuaSnip",
      "saadparwaiz1/cmp_luasnip",
      "rafamadriz/friendly-snippets",
    },
    lazy = false,
    config = function()
      -----------------------------
      --- Set up Snippets START ---
      -----------------------------

      -- Snippets
      -- VS Code like Snippets
      require("luasnip.loaders.from_vscode").lazy_load()

      -- Keymaps
      local ls = require("luasnip")
      vim.keymap.set({ "i", "s" }, "<C-l>", function()
        ls.jump(1)
      end, { silent = true })
      vim.keymap.set({ "i", "s" }, "<C-h>", function()
        ls.jump(-1)
      end, { silent = true })

      ---------------------------
      --- Set up Snippets END ---
      ---------------------------

      -----------------------------
      --- Set up nvim-cmp START ---
      -----------------------------

      local cmp = require("cmp")
      cmp.setup({
        -- Snippets Engine
        snippet = {
          expand = function(args)
            require("luasnip").lsp_expand(args.body)
          end,
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        -- Keymaps
        mapping = cmp.mapping.preset.insert({
          ["<C-j>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
          ["<C-k>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
          -- ["<Down>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
          -- ["<Up>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
          ["<C-b>"] = cmp.mapping.scroll_docs(-4),
          ["<C-f>"] = cmp.mapping.scroll_docs(4),
          ["<C-Space>"] = cmp.mapping.complete(),
          ["<C-e>"] = cmp.mapping.abort(),
          ["<C-y>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
        }),
        -- Where it gets the snippers from
        sources = {
          { name = "supermaven" },
          -- { name = "copilot" },
          { name = "nvim_lsp" },
          { name = "luasnip" },
          { name = "buffer" },
          { name = "path" },
        },
      })

      --------------------------
      --- Setup nvim-cmp END ---
      --------------------------

      ------------------------
      --- Set up LSP START ---
      ------------------------

      -- Added UI-Element to show status of LSP (Lower right hand corner)
      require("fidget").setup({})

      --- Variable is used to add autocompletion/snippets to server
      local capabilities = require("cmp_nvim_lsp").default_capabilities()
      -- Order of initialization is important
      -- Mason -> Mason-LSP -> LSPconfig
      -- In this case Mason-LSP calls the LSPconfig setup in the default handler
      require("mason").setup({})

      -- Setup the Language Servers with Plugin (Download via Mason)
      require("mason-tool-installer").setup({
        ensure_installed = {
          -- ADD LANGUAGES LINTERS and FORMATTERS HERE
          -- Language Servers
          "lua_ls",
          "rust_analyzer",
          "yamlls",
          "clangd",
          "taplo",
          "pyright",
          "bash-language-server",
          "terraform-ls",
          "gopls",
          "htmx-lsp",
          "tailwindcss-language-server",
          "marksman",
          -- Linters
          "luacheck",
          "shellcheck", -- Bash
          "tflint", -- Terraform
          "ruff",
          "golangci-lint",
          "stylelint",
          "htmlhint",
          "markdownlint",
          "mdformat",
          -- "pylint", -- Use pylint installed in local venv insdead
          -- Formatters
          "stylua",
          "isort",
          "black",
          "prettier",
          "beautysh", -- Bash
          "hclfmt", -- Terraform
          "goimports", -- Go
          -- Debuggers
          "cpptools",
          "codelldb",
          "debugpy",
          "delve", -- Go
        },
        -- auto_update = true,
      })

      -- Setup the Language Servers
      require("mason-lspconfig").setup({
        handlers = {
          -- disalbe ruff as lsp
          ["ruff"] = function() end,
          -- Auto "activates" the LSP-Server
          function(server_name) -- default handler (optional)
            require("lspconfig")[server_name].setup({
              capabilities = capabilities,
            })
          end,
        },
      })

      ----------------------
      --- Set up LSP END ---
      ----------------------
    end,
  },

  ----------------------------
  --- Set up Copilot START ---
  ----------------------------

  -- {
  --   "zbirenbaum/copilot.lua",
  --   event = { "InsertEnter" },
  --   dependencies = {
  --     "zbirenbaum/copilot-cmp",
  --   },
  --   config = function()
  --     require("copilot").setup({
  --       suggestion = {
  --         auto_trigger = true,
  --         keymap = {
  --           accept = "<C-z>",
  --           next = "<C-l>",
  --           prev = "<C-h>",
  --         },
  --       },
  --     })
  --     -- require("copilot").setup({
  --     --   suggestion = { enabled = true, auto_trigger = true },
  --     --   panel = { enabled = false },
  --     -- })
  --     -- require("copilot_cmp").setup()
  --   end,
  -- },

  {
    "supermaven-inc/supermaven-nvim",
    config = function()
      require("supermaven-nvim").setup({
        keymaps = {
          -- accept_suggestion = "<C-z>",
          -- clear_suggestion = "<C-e>",
          -- accept_word = "<C-S-l>",
        },
        log_level = "off",
        disable_inline_completion = true, -- disables inline completion for use with cmp
      })
    end,
  },

  --------------------------
  --- Set up Copilot END ---
  --------------------------
}
