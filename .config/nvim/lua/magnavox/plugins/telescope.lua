-- Telescope (Filefinder, fuzzy find)
return {
  -- For native fzf support
  { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
  -- Telescope itself
  {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = { "nvim-lua/plenary.nvim", "nvim-telescope/telescope-ui-select.nvim" },
    -- Setup
    config = function()
      -- Function to read ignore patterns from a .telescopeignore in the project root
      local function get_telescope_ignore_patterns_from_file()
        local ignore_file = vim.fn.getcwd() .. "/.telescopeignore"
        local ignore_patterns = { ".git/[^h]" }
        if vim.fn.filereadable(ignore_file) == 1 then
          for line in io.lines(ignore_file) do
            if line and line ~= "" then
              table.insert(ignore_patterns, line)
            end
          end
        end
        return ignore_patterns
      end

      require("telescope").setup({
        defaults = {
          scroll_strategy = "limit",
          -- Includes dot files in search
          -- file_ignore_patterns = { ".git/[^h]" },
          file_ignore_patterns = get_telescope_ignore_patterns_from_file(),
        },
        extensions = {
          ["ui-select"] = {
            require("telescope.themes").get_dropdown({}),
          },
          fzf = {
            fuzzy = true, -- false will only do exact matching
            override_generic_sorter = true, -- override the generic sorter
            override_file_sorter = true, -- override the file sorter
            case_mode = "smart_case", -- or "ignore_case" or "respect_case"
            -- the default case_mode is "smart_case"
          },
        },
      })

      local builtin = require("telescope.builtin")
      vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
      vim.keymap.set("n", "<leader>fg", builtin.git_files, {})
      vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
      vim.keymap.set("n", "<leader>fw", builtin.grep_string, {})
      vim.keymap.set("n", "<leader>fl", builtin.live_grep, {})
      vim.keymap.set("n", "<leader>fh", function()
        builtin.find_files({
          hidden = true, -- Include hidden files
          no_ignore = true, -- Disable ignoring patterns
          no_ignore_parent = true, -- Disable parent directory ignore patterns
          file_ignore_patterns = {}, -- Temporarily clear ignore patterns
        })
      end)

      -- To get ui-select loaded and working with telescope, you need to call
      -- load_extension, somewhere after setup function:
      require("telescope").load_extension("ui-select")
      -- To get fzf loaded and working with telescope, you need to call
      -- load_extension, somewhere after setup function:
      require("telescope").load_extension("fzf")
    end,
  },
}
